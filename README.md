## How do I get set up? ##

copy `config.php.template` file to `config.php`

### Create your app in vk.com ###

* create your app in `https://vk.com/apps?act=manage`
* copy app secret key to `VK_SECRET` in `config.php`
* copy app id to `VK_APP_ID` in `config.php`

### Get user token ###

* execute `php main.php —url`
* copy url from stdout to browser
* login and add application to account
* copy token=`<this>` from browser url to `config.php` as `VK_TOKEN`

### How to run it? ###

* execute `php main.php`
* enjoy