<?php
require_once dirname(__FILE__)."/vk/VkBot.php";

require_once dirname(__FILE__)."/listeners/FaceListener.php";
require_once dirname(__FILE__)."/listeners/SearchListener.php";
require_once dirname(__FILE__)."/listeners/WeatherListener.php";
require_once dirname(__FILE__)."/listeners/GuanoListener.php";
require_once dirname(__FILE__)."/listeners/KatogramListener.php";
require_once dirname(__FILE__)."/listeners/BotListener.php";

class Botinok extends VkBot {
	
	function __construct($token, $config) {
		parent::__construct($token);
		
		$this->registerListener(new FaceListener());
		
		if (isset($config['googleSearch'])) {
			$this->registerListener(new SearchListener($config['googleSearch']));
		}
		if (isset($config['weather'])) {
			$this->registerListener(new WeatherListener($config['weather']));
		}
		$this->registerListener(new GuanoListener());
		$this->registerListener(new KatogramListener());
		
		$this->registerListener(new BotListener());
	}
}