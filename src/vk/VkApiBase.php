<?php

class VkApiBase {
	const VK_API_VERSION = '5.52';

    protected $token;
    protected $curl;

    function __construct($token) {
        $this->token = $token;

        $this->curl = curl_init();
        curl_setopt_array($this->curl, array(
            CURLOPT_CONNECTTIMEOUT => 10,
            CURLOPT_HEADER => false,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_SSL_VERIFYHOST => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => false,
        ));
    }

    function __destruct() {
        curl_close($this->curl);
    }

    public function invokeMethod($method, $params) {
        $params['access_token'] = $this->token;
        $params['v'] = VkApiBase::VK_API_VERSION;
        curl_setopt_array($this->curl, array(
            CURLOPT_URL => 'https://api.vk.com/method/'. $method .'?' . http_build_query($params),
        ));
        return json_decode(curl_exec($this->curl), true);
    }
}