<?php 
require_once dirname(__FILE__)."/VkApi.php";
require_once dirname(__FILE__)."/VkBotListener.php";

class LongPoolServer {
    public $url;
    public $key;
    public $ts;

    function __construct($data) {
        $this->url = $data['server'];
        $this->key = $data['key'];
        $this->ts = $data['ts'];
    }
}

class VkBot extends VkApi {
	protected $listeners = [];
	
	public function registerListener(VkBotListener $listener) {
		if (!isset($this->listeners[$listener->getEventType()])) {
			$this->listeners[$listener->getEventType()] = [];
		}
		$this->listeners[$listener->getEventType()][] = $listener;
	}

    public function getLongPoolUpdates($server, $timeout) {
        $params = [
            'act' => 'a_check',
            'key' => $server->key,
            'ts' => $server->ts,
            'wait' => $timeout ?: '25',
            //'mode' => '2',
        ];
        curl_setopt_array($this->curl, array(
            CURLOPT_URL => 'https://'. $server->url .'?'. http_build_query($params),
        ));

        $response = json_decode(curl_exec($this->curl), true);
        $server->ts = $response['ts'] ?: $server->ts;
        return $response['updates'];
    }
	
    public function listen() {
        $server = $this->getLongPollServer();
        while ($server) {
            $updates = $this->getLongPoolUpdates($server, 25);
			if (!$updates) continue;

            foreach ($updates as $update) {
				if (isset($this->listeners[$update[0]])) {
					$i = 0;
					$listener = null;
					do {
						$listener = $this->listeners[$update[0]][$i];
						$i++;
					} while ($listener && !$listener->execute($this, $update));
				}
            }
            gc_collect_cycles();
        }
    }
}